# README #

Complile time string encryption by [Elbeno](https://github.com/elbeno).

### How do I get set up? ###

Just add the three header files to any project and include "strenc.h" to source files with strings you wish to encrypt.

* *strenc* - decrypts text into std::string
* *charenc* - decrypts text into const char*