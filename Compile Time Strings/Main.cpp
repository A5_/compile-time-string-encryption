#include <iostream>

#include "strenc.h"

int main()
{
	//	strenc   - decrypts text into std::string
	std::cout << strenc( "strenc   - decrypts text into std::string" ) << std::endl;
	
	//	charenc  - decrypts text into const char*
	std::cout << charenc( "charenc  - decrypts text into const char*" ) << std::endl;
}